from tkinter import *

width = 100
height= 300

class MainGUI:
    def RedLight(self):
        self.canvas.delete("red light")
        if self.v.get() ==1:
                self.canvas.create_oval(10, 20, 100, 100, fill = "green", tags = "red light")
                self.canvas.create_oval(10, 190, 100, 100, fill = "white", tags = "none")
                self.canvas.create_oval(10, 280, 100, 190, fill = "white", tags = "none")
        elif self.v.get() == 2:
            self.canvas.create_oval(10, 190, 100, 100, fill = "yellow", tags = "yellow light")
            self.canvas.create_oval(10, 280, 100, 190, fill = "white", tags = "none")
            self.canvas.create_oval(10, 20, 100, 100, fill = "white", tags = "none")
        elif self.v.get() == 3:
            self.canvas.create_oval(10, 280, 100, 190, fill = "red", tags = "red light")
            self.canvas.create_oval(10, 20, 100, 100, fill = "white", tags = "none")
            self.canvas.create_oval(10, 190, 100, 100, fill = "white", tags = "none")
                    
        
    def left(self):
        self.canvas.move("text", -5, 0)
    
    def right(self):
        self.canvas.move("text", 5, 0)
                
    def __init__(self):
        window = Tk()
        window.title("Stoplight")
            
        self.canvas = Canvas(window, bg = "white", width = width, height = height)
        self.canvas.create_rectangle(10, 280, 100, 20, tags = "shape")
        self.canvas.create_oval(10, 280, 100, 190, tags = "shape")
        self.canvas.create_oval(10, 190, 100, 100, tags = "shape")
        self.canvas.create_oval(10, 20, 100, 100, tags = "shape")
        self.canvas.pack()
        
        frame1 = Frame(window)
        frame1.pack()
        
        self.v = IntVar()
        Radiobutton(frame1, text = "Green", variable = self.v, value = 1, command = self.RedLight).pack(side = LEFT)
        Radiobutton(frame1, text = "Yellow", variable = self.v, value = 2, command = self.RedLight).pack(side = LEFT)
        Radiobutton(frame1, text = "Red", variable = self.v, value = 3, command = self.RedLight).pack(side = LEFT)
        window.mainloop()
MainGUI()
